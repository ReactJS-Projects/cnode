import React from 'react'
import {
  observer,
  inject,
} from 'mobx-react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Tabs, { Tab } from 'material-ui/Tabs'
import List from 'material-ui/List'
import { CircularProgress } from 'material-ui/Progress'
import queryString from 'query-string'

import AppState from '../../store/app-state'
import Container from '../layout/container'
import TopicListItem from './list-item'
import { tabs } from '../../utils/variable-define'

@inject((stores) => { // stores代表 Provider 上的所有属性
  return {
    appState: stores.appState,
    topicStore: stores.topicStore,
  }
}) @observer
export default class TopicList extends React.Component {
  static contextTypes = {
    router: PropTypes.object,
  }
  constructor() {
    super()
    this.changeTab = this.changeTab.bind(this)
    this.listItemClick = this.listItemClick.bind(this)
  }

  componentDidMount() {
    const tab = this.getTab()
    this.props.topicStore.fetchTopics(tab)
  }
  componentWillReceiveProps(nextProps) {
    const { search } = nextProps.location
    if (search !== this.props.location.search) {
      const tab = this.getTab(search)
      this.props.topicStore.fetchTopics(tab)
    }
  }

  getTab(search) {
    search = search || this.props.location.search
    const query = queryString.parse(search)
    return query.tab || 'all'
  }

  asyncBootstrap() { // 执行完这个函数之后才会渲染代码，能很好的处理初始化数据
    return new Promise((resolve) => {
      setTimeout(() => {
        this.props.appState.count = 1
        resolve(true)
      })
    })
  }

  changeTab(e, value) {
    console.log(value, 58)
    this.context.router.history.push({
      pathname: '/list',
      search: `?tab=${value}`,
    })
  }

  /* eslint-disable */
  listItemClick() {
    // ddd
  }
  /* eslint-disable */

  render() {

    const {
      topicStore,
    } = this.props

    const topicList = topicStore.topics
    const syncingTopics = topicStore.syncing
    const tab = this.getTab()
    // }
    return (
      <Container>
        <Helmet>
          <title>topic list</title>
          <meta name="description" content="this is description" />
        </Helmet>
        <Tabs value={tab} onChange={this.changeTab}>
          {
            Object.keys(tabs).map((item) => <Tab key={item} label={tabs[item]} value={item} />)
          }
        </Tabs>
        <List>
          {
            topicList.map((topic, index) => <TopicListItem key={index} onClick={this.listItemClick} topic={topic} />)
          }
          {
            syncingTopics ?
            (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center"
                }}
              ><CircularProgress size={100} /></div>
            ) : null
          }
        </List>
      </Container>
    )
  }
}

TopicList.wrappedComponent.propTypes = { // 验证mobx中的属性需要wrappedComponent
  appState: PropTypes.instanceOf(AppState).isRequired,
  topicStore: PropTypes.object.isRequired,
}

TopicList.propTypes = {
  location: PropTypes.object.isRequired,
}
