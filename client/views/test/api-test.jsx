import React from 'react'
import axios from 'axios'

/* eslint-disable */
export default class TestApi extends React.Component {
  getTopics() {
    axios.get('/api/topics').then(res => {
      console.log(res)
    }).catch(e => {
      console.log(e)
    })
  }

  login() {
    axios.post('/api/user/login', {
      accessToken: ' e4fa6b7a-0ce0-45a0-bcca-18bd17d6ea86'
    }).then(resp => {
      console.log(resp)
    }).catch(e => {
      console.log(e)
    })
  }

  markAll() {
    axios.post('/api/message/mark_all?needAccessToken=true').then(res => {
      console.log(res)
    }).catch(e => {
      console.log(e)
    })
  }

  render() {
    return (
      <div>
        <button onClick={this.getTopics}>topics</button>
        <button onClick={this.login}>login</button>
        <button onClick={this.markAll}>markAll</button>
      </div>
    )
  }
}
/* eslint-disable */
