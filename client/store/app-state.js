import {
  observable,
  computed,
  action,
  // autorun,
} from 'mobx'

export default class AppState {
  constructor({ count, name } = { count: 0, name: 'Jokcy' }) {
    this.count = count
    this.name = name
  }

  @observable count
  @observable name
  @computed get msg() {
    return `${this.name} say count is ${this.count}`
  }

  @action add() {
    this.count += 1
  }

  @action changeName(name) {
    this.name = name
  }

  toJson() { // 服务器渲染时，将AppState实例以json的格式拿到
    return {
      count: this.count,
      name: this.name,
    }
  }
}
