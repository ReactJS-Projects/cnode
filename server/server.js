// 生产环境

const express = require('express')
// const ReactSSR = require('react-dom/server')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const session = require('express-session')
const fs = require('fs')
const path = require('path')
const serverRender = require('./util/server-render')
const isDev = process.env.NODE_ENV === 'development'

const app = express()

app.use(bodyParser.json()) // 把请求过来的数据转化成req.body上的数据
app.use(bodyParser.urlencoded({ extended: false })) // 将表单提交过来的数据转化为req.body上的数据
app.use(session({ // 正式上线一般是放在数据库当中缓存
  maxAge: 10 * 60 * 1000,
  name: 'tid',
  resave: false,
  saveUninitialized: false,
  secret: 'react cnode class' // 随便写一个字符串，用这个字符串去加密cookie，保证浏览器端没法被人解密
}))
app.use(favicon(path.join(__dirname, '../favicon.ico')))
app.use('/api/user', require('./util/handle-login'))
app.use('/api', require('./util/proxy'))

if (!isDev) {
  const serverEntey = require('../dist/server-entry')
  // 获取index.tpl.html模板
  // const template = fs.readFileSync(path.join(__dirname, '../dist/index.html'), 'utf8')
  const template = fs.readFileSync(path.join(__dirname, '../dist/server.ejs'), 'utf8')

  // 用于区分那些是静态文件那些是动态文件
  app.use('/public', express.static(path.join(__dirname, '../dist')))

  // 从浏览器端发出的任何请求，都让他返回服务端渲染的代码
  app.get('*', function (req, res, next) {
    serverRender(serverEntey, template, req, res).catch(next)
  })
} else {
  const devStatic = require('./util/dev-static')
  devStatic(app)
}

// 全局定义处理err错误的中间件。
// 不管有没有用到后面的三个参数都要把它传递进来，因为exports会根据参数的长度来判断这是不是一个errorHandle中间件.
// 如果是才会使用errorHandle来处理这个中间件
app.use(function (error, req, res, next) {
  console.log(error)
  res.status(500).send(error)
})

app.listen(4444, function () {
  console.log('server is listening on 4444')
  console.log('server http://localhost:4444')
})
