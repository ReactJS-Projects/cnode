// 将数据存在session里面，需要登陆的接口都使用这些数据

const router = require('express').Router()
const axios = require('axios')

const baseUrl = 'http://cnodejs.org/api/v1'

router.post('/login', function (req, res, next) {
  axios.post(`${baseUrl}/accesstoken`, {
    accesstoken: req.body.accessToken
  }).then(resp => {
    if (resp.status === 200 && resp.data.success) {
      req.session.user = {
        accessToken: req.body.accessToken,
        loginName: resp.data.loginname,
        id: resp.data.id,
        avatarUrl: resp.data.avatal_url
      }
      res.json({
        success: true,
        data: resp.data
      })
    }
  }).catch(e => {
    if (e.response) {
      res.json({
        success: false,
        data: e.response.data
      })
    } else {
      next(e)
    }
  })
})

module.exports = router
