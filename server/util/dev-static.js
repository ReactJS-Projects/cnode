// 在开发环境中index.html文件是存在内存中的，无法直接获取到
// 所以需要使用axios从webpack服务中请求到index.html模板文件
// 这样就可以实时拿到打包后的index.html模板文件了
const axios = require('axios')
const webpack = require('webpack')
const path = require('path')
// 读写内存中的文件，类似于fs（fs读写的是硬盘中的文件）
const MemoryFs = require('memory-fs')
const proxy = require('http-proxy-middleware')

const serverRender = require('./server-render')
const serverConfig = require('../../build/webpack.config.server')

const getTemplate = () => {
  return new Promise((resolve, reject) => {
    axios.get('http://localhost:8888/public/server.ejs').then(res => {
      resolve(res.data)
    }).catch(reject)
  })
}

// module就是module.exports中的module，属于原生模块
const NativeModule = require('module')
const vm = require('vm')

const getModuleFromString = (bundle, filename) => {
  const m = { exports: {} }
  // module.wrap 将一个可执行的js代码包装成类似于 `(function(exports, require, module, __filename, __dirname){ ...bundle code })`
  const wrapper = NativeModule.wrap(bundle)
  // 通过new vm.Script 就可以执行包装之后的字符串代码
  // 参考 https://www.cnblogs.com/rubylouvre/archive/2011/11/25/2262521.html
  const script = new vm.Script(wrapper, {
    filename: filename,
    displayErrors: true // 有错误信息输出出来
  })
  // runInThisContext 相当于一个全新的环境中执行代码，不会影响当前作用域的对象。
  const result = script.runInThisContext()
  // m.exports作为一个调用者去调用result代码
  // m.exports对应module.wrap包装后的字符函数中的exports参数
  // require -> require
  // m -> module
  result.call(m.exports, m.exports, require, m)
  return m
}

const mfs = new MemoryFs
// 通过webpack启动一个webpaack服务
const serverCompiler = webpack(serverConfig)
let serverBundle
serverCompiler.outputFileSystem = mfs

serverCompiler.watch({}, (err, stats) => {
  // stats  webpack在打包过程中所输出的一些信息
  if (err) throw err
  stats = stats.toJson()
  stats.errors.forEach(err => console.error(err))
  stats.warnings.forEach(warn => console.warn(warn))

  // 获取服务端的bundle路径
  const bundlePath = path.join(
    serverConfig.output.path,
    serverConfig.output.filename
  )
  const bundle = mfs.readFileSync(bundlePath, 'utf-8')

  // 用module去解析js string的内容去生成一个新的模块
  const m = getModuleFromString(bundle, 'server-entry.js') // 指定文件名
  serverBundle = m.exports
})

module.exports = function (app) {
  app.use('/public', proxy({
    target: 'http://localhost:8888'
  }))

  app.get('*', function (req, res, next) {
    if (!serverBundle) {
      return res.send('waiting for compile, refresh later')
    }
    getTemplate().then(template => {
      return serverRender(serverBundle, template, req, res)
    }).catch(next)
  })
}
