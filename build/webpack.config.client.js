const path = require('path')
const webpack = require('webpack')
// 用于合并webpack配置
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const HTMLWebpackePlugin = require('html-webpack-plugin')
const OpenBrowserPlugin = require('open-browser-webpack-plugin')
const NameAllModulesPlugin = require('name-all-modules-plugin')

const cdnConfig = require('../app.config.js').cdn

const isDev = process.env.NODE_ENV === 'development'

const config = webpackMerge(baseConfig, {
  entry: {
    app: path.join(__dirname, '../client/app.js')
  },
  output: {
    filename: '[name].[hash].js'
  },
  plugins: [
    new HTMLWebpackePlugin({
      template: path.join(__dirname, '../client/index.tpl.html')
    }),
    new HTMLWebpackePlugin({
      template: '!!ejs-compiled-loader!' + path.join(__dirname, '../client/server-index.tpl.ejs'),
      filename: 'server.ejs'
    })
  ]
})

// webpack-dev-server  是在dist目录下创建的，而在output文件中配置了publicPath: 'public'，
// 所以实际路径为localhost:port/filename [localhost:8888/public]。
// 需要在devServer 中设置publicPath和historyApiFallback 才能将路径改为localhost:filename [localhost:8888/public]。
if (isDev) {
  // config.devtool = '#cheap-module-eval-source-map' // 在调试的时候显示的是未打包之前的源代码
  config.devtool = 'source-map'
  config.entry = [
    'react-hot-loader/patch',
    path.join(__dirname, '../client/app.js')
  ]
  config.devServer = {
    host: '0.0.0.0', // 可以使用ip、127.9.9.1或localhost访问
    port: '8888',
    // contentBase: path.join(__dirname, '../dist'),
    hot: true,
    overlay: { // 如果程序在编译过程中报错，直接将错误显示在网页上
      errors: true
    },
    publicPath: '/public/',
    historyApiFallback: {
      index: '/public/index.html' // 所有404的请求都返回设置的这个html文件
    },
    proxy: {
      '/api': 'http://localhost:4444'
    }
  }
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({
      url: 'http://localhost:8888'
    })
  )
} else {
  config.entry = {
    app: path.join(__dirname, '../client/app.js'),
    vendor: [
      'react',
      'react-dom',
      'react-router-dom',
      'mobx',
      'mobx-react',
      'axios',
      'query-string'
      // 'dateformat',
      // 'marked'
    ]
  }
  config.output.filename = '[name].[chunkhash].js'
  config.output.publicPath = cdnConfig.host
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({ // 将第三方包单独分割打包
      name: 'vendor'
    }),
    new webpack.optimize.CommonsChunkPlugin({ // 即使没有更改任何业务代码，只要重新打包webpack都会在app中生成代码，所以需要将webpack生成的代码单独分割打包
      name: 'manifest',
      minChunks: Infinity
    }),
    new webpack.NamedModulesPlugin(), // webpack异步加载
    new NameAllModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.NamedChunksPlugin((chunk) => { // 具体给每一个chunk打包所命名的名字
      if (chunk.name) {
        return chunk.name
      }
      return chunk.mapModules(m => path.relative(m.context, m.request)).join('_')
    })
  )
}

module.exports = config
