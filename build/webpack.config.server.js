const path = require('path')
const webpackMerge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const webpack = require('webpack')

module.exports = webpackMerge(baseConfig, {
  // 默认是在'web'端执行 "web" | "webworker" | "node" | "async-node" | "node-webkit" | "atom" | "electron" | "electron-main" | "electron-renderer" | function
  target: 'node',
  entry: {
    app: path.join(__dirname, '../client/server-entry.js')
  },
  externals: Object.keys(require('../package.json').dependencies), // 解决依赖被多次初始化问题。将依赖过滤掉，服务端是运行在node环境下的不需要全部引入，会自动require需要的文件
  output: {
    filename: 'server-entry.js',
    libraryTarget: 'commonjs2'
  },
  plugins: [
    new webpack.DefinePlugin({ // 使用DefinePlugin去定义变量，如果值是字符串需要先用单引号包裹然后用双引号包裹
      'process.env.API_BASE': '"http://127.0.0.1:4444"' // 服务端请求，需要发送到它本地的地址，因为是服务端在启动这个服务
    })
  ]
})
