react16新特新

  error boundary 错误

  New render return types 不需要根节点，可以直接使用数组或者字符串

  portals 通过createPortal创建节点，可以直接插入到某个节点中
  https://reactjs.org/docs/portals.html
  例子：
    class App extends Component{
      render(){
        return createPortal(
          this.props.children,
          document.querySelector('body')
        )
      }
    }
    const Child = () => (
      <p>this is child</p>
    )
    render(
      <App>
        <Child />
      </App>,
      document.querySelector('#root')
    )

  Better server-side rendering 
